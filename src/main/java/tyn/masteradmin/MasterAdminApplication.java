package tyn.masteradmin;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import tyn.masteradmin.infrastructure.core.InitDB;

@SpringBootApplication
@EnableDiscoveryClient
@RequiredArgsConstructor
public class MasterAdminApplication {

	@Autowired
	private final InitDB initDB;

	public static void main(String[] args) {
		SpringApplication.run(MasterAdminApplication.class, args);
	}

}
