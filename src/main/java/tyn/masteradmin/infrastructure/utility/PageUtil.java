package tyn.masteradmin.infrastructure.utility;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;
import tyn.masteradmin.infrastructure.core.FindAllBaseRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.util.Map.entry;

public class PageUtil {

    public static PageRequest getPageRequest(FindAllBaseRequest request) {
        List<String> sortList = null;
        if (StringUtils.isNotEmpty(request.getSort())) {
            sortList = Arrays.asList(request.getSort().split(","));
        }
        if (request.getPage() == null || request.getPage() == 0) {
            request.setPage(1);
        }
        if (request.getTake() == null || request.getTake() == 0) {
            request.setTake(10);
        }
        return PageRequest.of(request.getPage() - 1, request.getTake(), getSort(sortList));
    }

    public static Sort getSort(List<String> sortList) {
        Map<String, String> sortMap = Map.ofEntries(
                entry("id", "id"),
                entry("createdAt", "createdAt"),
                entry("updatedAt", "updatedAt")
        );

        if (CollectionUtils.isEmpty(sortList)) {
            return Sort.by(List.of(Sort.Order.desc("updatedAt")));
        }

        List<Sort.Order> orders = new ArrayList<>();
        for (String sort : sortList) {
            if (sort.startsWith("-")) {
                String key = sort.substring(1);
                orders.add(Sort.Order.desc(sortMap.getOrDefault(key, key)));
            } else {
                orders.add(Sort.Order.asc(sortMap.getOrDefault(sort, sort)));
            }
        }
        return Sort.by(orders);
    }
}
