package tyn.masteradmin.infrastructure.utility;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import java.util.List;

public class SpecificationUtil {

    public static <T> Specification<T> andSpec(List<Specification<T>> condition) {
        Specification<T> spec = null;
        if (!CollectionUtils.isEmpty(condition)) {
            spec = Specification.where(condition.get(0));
            for (int i = 1; i < condition.size(); i++) {
                spec = spec.and(condition.get(i));
            }
        }
        return spec;
    }

}
