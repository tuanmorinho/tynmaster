package tyn.masteradmin.infrastructure.utility;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import tyn.masteradmin.infrastructure.config.exception.RestException;

public class Utils {

    public static String convertObjectToJson(Object object) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RestException("error.internal_server_error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
