package tyn.masteradmin.infrastructure.core;

import lombok.Data;

@Data
public class FindAllBaseRequest {
    private Integer page;
    private Integer take;
    private String sort;
}
