package tyn.masteradmin.infrastructure.core;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

@MappedSuperclass
@Data
public abstract class BaseEntity implements Serializable {

    @CreationTimestamp
    @Column(name = "createdAt", nullable = false, updatable = false)
    protected ZonedDateTime createdAt;

    @CreationTimestamp
    @Column(name = "updatedAt", nullable = false)
    protected ZonedDateTime updatedAt;

}
