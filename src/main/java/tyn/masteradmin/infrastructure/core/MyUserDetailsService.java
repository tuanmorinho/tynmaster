package tyn.masteradmin.infrastructure.core;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import tyn.masteradmin.app.domain.Role;
import tyn.masteradmin.app.domain.User;
import tyn.masteradmin.app.repository.UserRepository;
import tyn.masteradmin.infrastructure.config.exception.RestException;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {
        final User userByUsername = userRepository.findByUsernameWithRoles(username)
                .orElseThrow(() ->
                        new RestException("error.unauthorized", HttpStatus.UNAUTHORIZED, username));

        final Set<Role> authorities = userByUsername.getRoles();

        final User user = User.builder()
                .id(userByUsername.getId())
                .firstName(userByUsername.getFirstName())
                .lastName(userByUsername.getLastName())
                .username(userByUsername.getUsername())
                .password(userByUsername.getPassword())
                .roles(authorities)
                .build();

        return new User(user);
    }
}
