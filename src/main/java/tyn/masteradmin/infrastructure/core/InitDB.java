package tyn.masteradmin.infrastructure.core;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import tyn.masteradmin.app.common.RoleEnum;
import tyn.masteradmin.app.domain.Meta;
import tyn.masteradmin.app.domain.Role;
import tyn.masteradmin.app.domain.User;
import tyn.masteradmin.app.repository.MetaRepository;
import tyn.masteradmin.app.repository.RoleRepository;
import tyn.masteradmin.app.repository.UserRepository;
import tyn.masteradmin.infrastructure.config.exception.RestException;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class InitDB {

    @Autowired
    private final UserRepository userRepository;
    @Autowired
    private final RoleRepository roleRepository;
    @Autowired
    private final MetaRepository metaRepository;
    @Autowired
    private final PasswordEncoder passwordEncoder;

    @PostConstruct
    private void initRoleAndAdminUser() {
        // init role
        roleRepository.deleteAll();
        List<RoleEnum> roleEnums = Arrays.asList(RoleEnum.values());
        List<Role> roles = roleEnums.stream()
                .map(role -> new Role(role.name(), "Phân quyền "+role.name().toLowerCase())).collect(Collectors.toList());
        roleRepository.saveAll(roles);

        final Role role = roleRepository.findByName(RoleEnum.ADMIN.name())
                .orElseThrow(() -> new RestException("error.register.role_not_found", HttpStatus.NOT_FOUND));

        // init user admin
        userRepository.deleteAll();
        User user = new User();
        user.setFirstName("TYN");
        user.setLastName("Cinplex");
        user.setUsername("admin");
        user.setPassword(passwordEncoder.encode("Admin123#@"));
        user.setRoles(Collections.singleton(role));
        userRepository.save(user);

        // init seo meta
        metaRepository.deleteAll();
        List<Meta> seoMeta = Stream.<Meta>builder()
                .add(new Meta(4, "Hệ thống rạp chiếu phim cao cấp hàng đầu Việt Nam", "rạp phim, phim ảnh", "Hệ thống rạp chiếu phim TYN Star Cineplex với chất lượng âm thanh &amp; hình ảnh hiện đại nhất. Đặt vé và nhận nhiều chương trình khuyến mãi hấp dẫn ngay hôm nay", "", ""))
                .build()
                .toList();
        metaRepository.saveAll(seoMeta);
    }
}