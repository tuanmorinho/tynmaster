package tyn.masteradmin.infrastructure.core;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
@RequiredArgsConstructor
public class MessageLocaleService {

    @Autowired
    private final MessageSource messageSource;

    public String getMessage(String messageId, Object... params) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(messageId, params, locale);
    }

    public String getMessage(String pattern, Locale locale, Object... params) {
        return messageSource.getMessage(pattern, params, locale);
    }
}
