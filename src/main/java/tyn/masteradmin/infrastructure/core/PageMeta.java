package tyn.masteradmin.infrastructure.core;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.domain.Sort;

@Data
@Builder
@AllArgsConstructor
public class PageMeta {
    private Integer page;
    private Integer take;
    private Long total;
    private Sort sort;
}
