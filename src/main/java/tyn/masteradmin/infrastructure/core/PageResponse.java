package tyn.masteradmin.infrastructure.core;

import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tyn.masteradmin.app.domain.Role;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

@Data
public class PageResponse<T, S> {
    List<S> content;
    PageMeta pageable;

    public PageResponse(Page<T> page, Class<T> classT, Class<S> classS) {
        final List<S> response = mappingReponse(page, classT, classS);
        final Pageable pageable = page.getPageable();
        this.content = response;
        this.pageable = PageMeta.builder()
                .page(pageable.getPageNumber())
                .take(pageable.getPageSize())
                .total(page.getTotalElements())
                .sort(pageable.getSort())
                .build();
    }

    private List<S> mappingReponse(Page<T> page, Class<T> classT, Class<S> classS) {
        return page.getContent().stream().map(data -> {
            try {
                return classS.getConstructor(classT).newInstance(data);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                     NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        }).toList();
    }
}
