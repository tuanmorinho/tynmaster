package tyn.masteradmin.infrastructure.core;

import jakarta.persistence.metamodel.SingularAttribute;
import org.springframework.data.jpa.domain.Specification;

public abstract class AbstractSpecification<T> {

    public <A> Specification<T> eq(SingularAttribute<T, A> field, A value) {
        return ((root, query, cb) -> cb.equal(root.get(field), value));
    }

    public <A> Specification<T> eq(SingularAttribute<T, A> first, SingularAttribute<T, A> second) {
        return ((root, query, cb) -> cb.equal(root.get(first), root.get(second)));
    }

}
