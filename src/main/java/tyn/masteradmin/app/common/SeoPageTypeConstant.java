package tyn.masteradmin.app.common;

public class SeoPageTypeConstant {
    public static final Integer MOVIE = 1;
    public static final Integer SHOW_TIME = 2;
    public static final Integer STORE = 3;
    public static final Integer LANDING = 4;
    public static final Integer PROMOTION = 5;
}
