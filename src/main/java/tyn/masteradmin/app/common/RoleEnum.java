package tyn.masteradmin.app.common;

public enum RoleEnum {
    USER,
    ADMIN,
    EDITOR,
    STAFF
}
