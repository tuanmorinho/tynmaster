package tyn.masteradmin.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import tyn.masteradmin.app.domain.Meta;

import java.util.Optional;

@Repository
public interface MetaRepository extends JpaRepository<Meta, Integer>, JpaSpecificationExecutor<Meta> {

    Optional<Meta> findByType(Integer type);

    Optional<Meta> findByObjectIdAndType(Integer objectId, Integer type);

    Optional<Meta> findByKeywordsLikeAndType(String keywords, Integer type);
}
