package tyn.masteradmin.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tyn.masteradmin.app.domain.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query("SELECT u FROM User u LEFT JOIN FETCH u.roles r WHERE u.username = :username")
    Optional<User> findByUsernameWithRoles(String username);

    @Query("SELECT u FROM User u LEFT JOIN FETCH u.roles r WHERE u.id = :id")
    Optional<User> findByIdWithRoles(Integer id);

    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    void deleteAllByUsernameNotLike(String username);

    void deleteAllByIdIn(List<Integer> ids);
}
