package tyn.masteradmin.app.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tyn.masteradmin.app.domain.Role;
import tyn.masteradmin.app.domain.User;

import java.util.*;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    Optional<Role> findByName(String name);

    Page<Role> findByNameContaining(String name, Pageable pageable);

    Set<Role> findByIdIn(List<Integer> ids);

    @Query("SELECT u FROM User u JOIN u.roles r WHERE r.name = :name")
    List<User> findAllUserByRole(String name);

    void deleteAllByNameNotLike(String name);
}
