package tyn.masteradmin.app.specification;

import org.springframework.stereotype.Component;
import tyn.masteradmin.app.domain.Meta;
import tyn.masteradmin.infrastructure.core.AbstractSpecification;

@Component
public class MetaSpecification extends AbstractSpecification<Meta> {
}
