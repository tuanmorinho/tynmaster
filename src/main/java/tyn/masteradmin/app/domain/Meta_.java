package tyn.masteradmin.app.domain;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;
import tyn.masteradmin.app.domain.Meta;

@StaticMetamodel(Meta.class)
public class Meta_ {
    public static volatile SingularAttribute<Meta, Integer> id;
    public static volatile SingularAttribute<Meta, Integer> objectId;
    public static volatile SingularAttribute<Meta, Integer> type;
    public static volatile SingularAttribute<Meta, String> keywords;
}
