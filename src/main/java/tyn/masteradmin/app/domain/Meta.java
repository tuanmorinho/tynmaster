package tyn.masteradmin.app.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tyn.masteradmin.infrastructure.core.BaseEntity;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tbl_seo_meta")
public class Meta extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer objectId;
    private Integer type;
    private String title;
    private String keywords;
    private String image;
    private String imageAlt;
    @Column(columnDefinition = "TEXT" ,nullable = false)
    private String description;

    public Meta(Integer objectId, Integer type, String title, String keywords, String description, String image, String imageAlt) {
        this.objectId = objectId;
        this.type = type;
        this.title = title;
        this.keywords = keywords;
        this.description = description;
        this.image = image;
        this.imageAlt = imageAlt;
    }

    public Meta(Integer type, String title, String keywords, String description, String image, String imageAlt) {
        this.type = type;
        this.title = title;
        this.keywords = keywords;
        this.description = description;
        this.image = image;
        this.imageAlt = imageAlt;
    }
}
