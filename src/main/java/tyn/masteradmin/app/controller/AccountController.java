package tyn.masteradmin.app.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tyn.masteradmin.app.dto.ProfileRequest;
import tyn.masteradmin.app.dto.ProfileResponse;
import tyn.masteradmin.app.service.AccountService;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {

    @Autowired
    private final AccountService accountService;

    @GetMapping("/getProfile/{id}")
    public ResponseEntity<ProfileResponse> getProfile(@PathVariable Integer id) {
        return accountService.getProfile(id);
    }

    @PostMapping("/updateProfile/{id}")
    public ResponseEntity<ProfileResponse> updateProfile(@PathVariable Integer id, @RequestBody ProfileRequest request) {
        return accountService.updateProfile(id, request);
    }
}
