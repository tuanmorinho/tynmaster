package tyn.masteradmin.app.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tyn.masteradmin.app.dto.SeoMetaRequest;
import tyn.masteradmin.app.dto.SeoMetaResponse;
import tyn.masteradmin.app.service.MetaService;

@RestController
@RequestMapping("/meta")
@RequiredArgsConstructor
public class MetaController {

    @Autowired
    private final MetaService metaService;

    @PostMapping("/getMetaConfig")
    public ResponseEntity<SeoMetaResponse> getPageMetaConfig(@RequestBody SeoMetaRequest request) {
        return metaService.getPageMetaConfig(request);
    }
}
