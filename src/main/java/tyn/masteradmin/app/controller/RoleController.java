package tyn.masteradmin.app.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tyn.masteradmin.app.domain.Role;
import tyn.masteradmin.app.dto.RolePageRequest;
import tyn.masteradmin.app.dto.RoleRequest;
import tyn.masteradmin.app.dto.RoleResponse;
import tyn.masteradmin.app.service.RoleService;
import tyn.masteradmin.infrastructure.core.PageResponse;

@RestController
@RequestMapping("/role")
@RequiredArgsConstructor
public class RoleController {

    @Autowired
    private final RoleService roleService;

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public ResponseEntity<PageResponse<Role, RoleResponse>> findRole(@RequestBody RolePageRequest request) {
        return roleService.findRole(request);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public ResponseEntity<RoleResponse> getRole(@PathVariable Integer id) {
        return roleService.getRole(id);
    }

    @PostMapping("/createRole")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<RoleResponse> createRole(@RequestBody RoleRequest request) {
        return roleService.createRole(request);
    }

    @PutMapping("/updateRole/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public ResponseEntity<RoleResponse> updateRole(@PathVariable Integer id, @RequestBody RoleRequest request) {
        return roleService.updateRole(id, request);
    }

    @DeleteMapping("/deleteAllRoles")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<HttpStatus> deleteAllRoles() {
        return roleService.deleteAllRoles();
    }

    @DeleteMapping("/deleteRole/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<RoleResponse> deleteRole(@PathVariable Integer id) {
        return roleService.deleteRole(id);
    }
}
