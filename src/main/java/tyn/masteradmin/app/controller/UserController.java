package tyn.masteradmin.app.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tyn.masteradmin.app.dto.AuthenticateRequest;
import tyn.masteradmin.app.dto.ProfileResponse;
import tyn.masteradmin.app.dto.UserRequest;
import tyn.masteradmin.app.service.AuthenticationService;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    @Autowired
    private final AuthenticationService authenticationService;

    @PostMapping("/createUser")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ProfileResponse> createUser(@RequestBody UserRequest request) {
        return authenticationService.createUser(request);
    }

    @PostMapping("/updateAuthority/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<HttpStatus> updateAuthority(@PathVariable Integer id, @RequestBody AuthenticateRequest request) {
        return authenticationService.updateAuthority(id, request);
    }

    @GetMapping("/internal/getUser/{username}")
    public ResponseEntity<ProfileResponse> getUser(@PathVariable String username) {
        return authenticationService.getUserInternal(username);
    }
}
