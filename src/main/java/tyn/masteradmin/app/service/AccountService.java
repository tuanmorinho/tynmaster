package tyn.masteradmin.app.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import tyn.masteradmin.infrastructure.config.exception.RestException;
import tyn.masteradmin.app.domain.User;
import tyn.masteradmin.app.dto.ProfileRequest;
import tyn.masteradmin.app.dto.ProfileResponse;
import tyn.masteradmin.app.repository.UserRepository;

@Service
@RequiredArgsConstructor
public class AccountService {

    @Autowired
    private final UserRepository userRepository;

    public ResponseEntity<ProfileResponse> getProfile(Integer id) {
        final User user = userRepository.findById(id)
                .orElseThrow(() -> new RestException("error.id_not_found", HttpStatus.NOT_FOUND, id));
        final ProfileResponse profileResponse = ProfileResponse
                .builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .username(user.getUsername())
                .build();
        return new ResponseEntity<>(profileResponse, HttpStatus.OK);
    }

    public ResponseEntity<ProfileResponse> updateProfile(Integer id, ProfileRequest request) {
        final User user = userRepository.findById(id)
                .orElseThrow(() -> new RestException("error.id_not_found", HttpStatus.NOT_FOUND, id));
        if (user.getUsername().equals("admin")) {
            throw new RestException("error.internal_server_error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        try {
            user.setFirstName(request.getFirstName());
            user.setLastName(request.getLastName());
            final User saveResponse = userRepository.save(user);
            return new ResponseEntity<>(new ProfileResponse(saveResponse), HttpStatus.OK);
        } catch (Exception e) {
            throw new RestException("error.internal_server_error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
