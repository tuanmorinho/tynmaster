package tyn.masteradmin.app.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tyn.masteradmin.app.common.SeoPageTypeConstant;
import tyn.masteradmin.app.domain.Meta;
import tyn.masteradmin.app.dto.SeoMetaRequest;
import tyn.masteradmin.app.dto.SeoMetaResponse;
import tyn.masteradmin.app.domain.Meta_;
import tyn.masteradmin.app.repository.MetaRepository;
import tyn.masteradmin.app.specification.MetaSpecification;
import tyn.masteradmin.infrastructure.config.exception.RestException;
import tyn.masteradmin.infrastructure.utility.SpecificationUtil;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class MetaService {

    @Autowired
    private final MetaRepository metaRepository;
    @Autowired
    private final MetaSpecification metaSpecification;

    public ResponseEntity<SeoMetaResponse> getPageMetaConfig(SeoMetaRequest request) {
        try {
            final List<Specification<Meta>> conditions = new ArrayList<>();

            if (request.getObjectId() != null) {
                conditions.add(metaSpecification.eq(Meta_.objectId, request.getObjectId()));
            }
            if (request.getType() != null) {
                conditions.add(metaSpecification.eq(Meta_.type, request.getObjectId()));
            }
            if (request.getKeywords() != null) {
                conditions.add(metaSpecification.eq(Meta_.keywords, request.getKeywords()));
            }
            if (CollectionUtils.isEmpty(conditions)) {
                conditions.add(metaSpecification.eq(Meta_.type, SeoPageTypeConstant.LANDING));
            }

            final Meta metaConfig = metaRepository.findOne(SpecificationUtil.andSpec(conditions))
                    .orElseThrow(() -> new RestException("error.not_found", HttpStatus.BAD_REQUEST));
            return new ResponseEntity<>(new SeoMetaResponse(metaConfig), HttpStatus.OK);
        } catch (Exception e) {
            throw new RestException("error.internal_server_error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
