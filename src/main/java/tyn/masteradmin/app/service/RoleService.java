package tyn.masteradmin.app.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tyn.masteradmin.app.common.RoleEnum;
import tyn.masteradmin.app.domain.Role;
import tyn.masteradmin.app.domain.User;
import tyn.masteradmin.app.dto.RolePageRequest;
import tyn.masteradmin.app.dto.RoleRequest;
import tyn.masteradmin.app.dto.RoleResponse;
import tyn.masteradmin.app.repository.RoleRepository;
import tyn.masteradmin.app.repository.UserRepository;
import tyn.masteradmin.infrastructure.config.exception.RestException;
import tyn.masteradmin.infrastructure.core.PageResponse;
import tyn.masteradmin.infrastructure.utility.PageUtil;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class RoleService {

    @Autowired
    private final RoleRepository roleRepository;
    @Autowired
    private final UserRepository userRepository;

    public ResponseEntity<PageResponse<Role, RoleResponse>> findRole(RolePageRequest request) {
        try {
            final Page<Role> roles = roleRepository.findByNameContaining(Objects.toString(request.getName()), PageUtil.getPageRequest(request));
            final PageResponse<Role, RoleResponse> response = new PageResponse<>(roles, Role.class, RoleResponse.class);
            if (response.getContent().isEmpty()) {
                return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            throw new RestException("error.internal_server_error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<RoleResponse> getRole(Integer id) {
        final Role role = roleRepository.findById(id)
                .orElseThrow(() -> new RestException("error.id_not_found", HttpStatus.BAD_REQUEST, id));
        return new ResponseEntity<>(new RoleResponse(role), HttpStatus.OK);
    }

    public ResponseEntity<RoleResponse> createRole(RoleRequest request) {
        try {
            final Role roleBody = Role.builder()
                    .name(request.getName().toUpperCase())
                    .description(request.getDescription())
                    .build();
            final Role saveResponse = roleRepository.save(roleBody);
            return new ResponseEntity<>(new RoleResponse(saveResponse), HttpStatus.OK);
        } catch (Exception e) {
            throw new RestException("error.internal_server_error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<RoleResponse> updateRole(Integer id, RoleRequest request) {
        final Role role = roleRepository.findById(id)
                .orElseThrow(() -> new RestException("error.id_not_found", HttpStatus.NOT_FOUND, id));
        if (role.getName().equals(RoleEnum.ADMIN.name())) {
            throw new RestException("error.role.cannot_update", HttpStatus.BAD_REQUEST, RoleEnum.ADMIN.name().toLowerCase());
        }
        try {
            role.setDescription(request.getDescription());
            final Role saveResponse = roleRepository.save(role);
            return new ResponseEntity<>(new RoleResponse(saveResponse), HttpStatus.OK);
        } catch (Exception e) {
            throw new RestException("error.internal_server_error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<HttpStatus> deleteAllRoles() {
        try {
            userRepository.deleteAllByUsernameNotLike("admin");
            roleRepository.deleteAllByNameNotLike(RoleEnum.ADMIN.name());
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            throw new RestException("error.internal_server_error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<RoleResponse> deleteRole(Integer id) {
        final Role role = roleRepository.findById(id)
                .orElseThrow(() -> new RestException("error.id_not_found", HttpStatus.NOT_FOUND, id));

        if (role.getName().equals(RoleEnum.ADMIN.name())) {
            throw new RestException("error.role.cannot_delete", HttpStatus.BAD_REQUEST, RoleEnum.ADMIN.name());
        }

        try {
            final List<Integer> userIdsDeleted = roleRepository.findAllUserByRole(role.getName())
                    .stream().map(User::getId).toList();
            userRepository.deleteAllByIdIn(userIdsDeleted);
            roleRepository.deleteById(id);
            return new ResponseEntity<>(new RoleResponse(id, null, null), HttpStatus.OK);
        } catch (Exception e) {
            throw new RestException("error.internal_server_error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
