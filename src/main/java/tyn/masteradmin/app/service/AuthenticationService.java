package tyn.masteradmin.app.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import tyn.masteradmin.app.domain.Role;
import tyn.masteradmin.app.dto.*;
import tyn.masteradmin.app.repository.RoleRepository;
import tyn.masteradmin.infrastructure.config.exception.RestException;
import tyn.masteradmin.app.common.RoleEnum;
import tyn.masteradmin.app.domain.User;
import tyn.masteradmin.app.repository.UserRepository;
import tyn.masteradmin.infrastructure.utility.JwtUtil;

import java.util.Collections;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    @Autowired
    private final UserRepository userRepository;
    @Autowired
    private final RoleRepository roleRepository;
    @Autowired
    private final PasswordEncoder passwordEncoder;
    @Autowired
    private final JwtUtil jwtUtil;
    @Autowired
    private final AuthenticationManager authenticationManager;

    public ResponseEntity<AuthenticationResponse> register(RegisterRequest request) {
        if (userRepository.existsByUsername(request.getUsername())) {
            throw new RestException("error.register.duplicate_username", HttpStatus.BAD_REQUEST);
        }

        final Role role = roleRepository.findByName(RoleEnum.USER.name())
                .orElseThrow(() -> new RestException("error.register.role_not_found", HttpStatus.NOT_FOUND));

        final User user = new User();
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setUsername(request.getUsername());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setRoles(Collections.singleton(role));
        userRepository.save(user);

        final String token = jwtUtil.generateToken(user);
        final AuthenticationResponse authResponse = AuthenticationResponse
                .builder()
                .token(token)
                .build();
        return new ResponseEntity<>(authResponse, HttpStatus.OK);
    }

    public ResponseEntity<AuthenticationResponse> authenticate(AuthenticateRequest request) {
        final User user = userRepository.findByUsernameWithRoles(request.getUsername())
                .orElseThrow(() -> new RestException("error.authenticate.user_not_found", HttpStatus.BAD_REQUEST));

        if (user.getUsername() == null || !user.getUsername().equals(request.getUsername())) {
            throw new RestException("error.authenticate.user_not_found", HttpStatus.BAD_REQUEST);
        }
        if (user.getPassword() == null || !passwordEncoder.matches(request.getPassword(), user.getPassword())) {
            throw new RestException("error.authenticate.user_not_found", HttpStatus.BAD_REQUEST);
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUsername(),
                        request.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        final String token = jwtUtil.generateToken((UserDetails) authentication.getPrincipal());
        final AuthenticationResponse authResponse = AuthenticationResponse
                .builder()
                .token(token)
                .build();
        return new ResponseEntity<>(authResponse, HttpStatus.OK);
    }

    public ResponseEntity<HttpStatus> updateAuthority(Integer id, AuthenticateRequest request) {
        final Role roleAdmin = roleRepository.findByName(RoleEnum.ADMIN.name())
                .orElseThrow(() -> new RestException("error.register.role_not_found", HttpStatus.NOT_FOUND));
        final User userByUsername = userRepository.findByIdWithRoles(id)
                .orElseThrow(() -> new RestException("error.id_not_found", HttpStatus.BAD_REQUEST, id));
        if (userByUsername.getRoles().contains(roleAdmin)) {
            throw new RestException("error.internal_server_error", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        final Set<Role> newRoles = roleRepository.findByIdIn(request.getRoleIds());
        if (newRoles.isEmpty()) {
            throw new RestException("error.id_not_found", HttpStatus.BAD_REQUEST, request.getRoleIds().stream().toList());
        }

        try {
            userByUsername.resetRole();
            newRoles.forEach(userByUsername::addRole);
            userRepository.save(userByUsername);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            throw new RestException("error.internal_server_error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<ProfileResponse> createUser(UserRequest request) {
        try {
            final Role role = roleRepository.findByName(RoleEnum.USER.name())
                    .orElseThrow(() -> new RestException("error.register.role_not_found", HttpStatus.NOT_FOUND));

            final User user = new User();
            user.setFirstName(request.getFirstName());
            user.setLastName(request.getLastName());
            user.setUsername(request.getUsername());
            user.setPassword(passwordEncoder.encode(request.getPassword()));
            user.setRoles(Collections.singleton(role));

            final User saveResponse = userRepository.save(user);
            return new ResponseEntity<>(new ProfileResponse(saveResponse), HttpStatus.OK);
        } catch (Exception e) {
            throw new RestException("error.internal_server_error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<ProfileResponse> getUserInternal(String username) {
        final User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new RestException("error.id_not_found", HttpStatus.BAD_REQUEST, username));
        return new ResponseEntity<>(new ProfileResponse(user), HttpStatus.OK);
    }
}