package tyn.masteradmin.app.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tyn.masteradmin.app.domain.User;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProfileResponse {
    private String firstName;
    private String lastName;
    private String username;

    public ProfileResponse(User response) {
        this.firstName = response.getFirstName();
        this.lastName = response.getLastName();
        this.username = response.getUsername();
    }
}
