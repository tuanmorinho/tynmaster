package tyn.masteradmin.app.dto;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import tyn.masteradmin.app.domain.Meta;

@Data
@AllArgsConstructor
public class SeoMetaResponse {

    private Integer id;
    private String title;
    private String keywords;
    private Integer type;
    private Integer objectId;
    private String description;
    private String image;
    private String imageAlt;

    public SeoMetaResponse(Meta response) {
        this.id = response.getId();
        this.title = response.getTitle();
        this.keywords = response.getKeywords();
        this.type = response.getType();
        this.objectId = response.getObjectId();
        this.description = response.getDescription();
        this.image = response.getImage();
        this.imageAlt = response.getImageAlt();
    }
}
