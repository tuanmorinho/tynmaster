package tyn.masteradmin.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import tyn.masteradmin.app.domain.Role;

@Data
@AllArgsConstructor
public class RoleResponse {
    private Integer id;
    private String name;
    private String description;

    public RoleResponse(Role response) {
        this.id = response.getId();
        this.name = response.getName();
        this.description = response.getDescription();
    }
}
