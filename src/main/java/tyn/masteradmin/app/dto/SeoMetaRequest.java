package tyn.masteradmin.app.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SeoMetaRequest {
    private Integer objectId;
    private String keywords;
    private Integer type;
}
